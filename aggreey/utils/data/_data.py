# -*- coding: utf-8 -*-

# imports
import json
import networkx as nx


# functions
def load_kialo_graph_data(file_path: str) -> nx.DiGraph:
    """

    Parameters
    ----------
    file_path: str
        Path to the Kialo JSON file.

    Returns
    -------
    graph: nx.Digraph
        NetworkX directed graph of the Kialo debate.
    """
    with open(file_path, 'r') as file:
        graph_data = json.load(file)

    graph = nx.DiGraph()

    for node_id, node_data in graph_data['nodes'].items():
        graph.add_node(
            node_id,
            **node_data
        )

    for source_id, edge_data in graph_data['edges'].items():
        graph.add_edge(
            source_id,
            edge_data['successor_id'],
            **{key: value for key, value in edge_data.items() if key != 'successor_id'}
        )

    return graph


if __name__ == '__main__':
    ...
