# -*- coding: utf-8 -*-

# imports


# classes
class Foo:
    def __init__(self):
        ...


# functions
def bar():
    ...


if __name__ == '__main__':
    ...
