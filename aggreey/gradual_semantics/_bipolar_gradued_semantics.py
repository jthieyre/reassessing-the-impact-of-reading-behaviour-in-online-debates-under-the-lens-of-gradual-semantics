# -*- coding: utf-8 -*-

# imports
import networkx as nx
import numpy as np

from typing import Any, Dict


# functions
def quad_semantics(graph: nx.DiGraph, initial_acceptability: Dict[Any, float], eps: float = 1e-4) -> Dict[Any, float]:
    """
    Compute Quantitative Argumentation Debate semantics from [1]_.

    Parameters
    ----------
    graph: nx.DiGraph
        Debate graph.

    initial_acceptability: dict
        Dictionary of initial nodes acceptabilities. Keys must be node IDs, and values are floats between 0 and 1.

    eps: float, default=1e-4
        Threshold used to determine if the final acceptability has converged.

    Returns
    -------
    final_acceptability: dict
        Dictionary of final nodes acceptabilities. Keys are the same as `initial_acceptability`, and values are floats
        between 0 and 1.

    References
    ----------

    .. [1] Baroni et al., Automatic evaluation of design alternatives with quantitative argumentation, 2015.
    """
    final_acceptability = initial_acceptability.copy()
    has_changed = True

    while has_changed:
        temp_acceptability = {}
        has_changed = False

        for node_id in graph.nodes:
            # Get lists of node attackers et supporters
            attackers = []
            supporters = []
            for predecessor_id in graph.predecessors(node_id):
                if graph.edges[(predecessor_id, node_id)]['relation'] == -1:
                    attackers.append(predecessor_id)
                elif graph.edges[(predecessor_id, node_id)]['relation'] == 1:
                    supporters.append(predecessor_id)
                elif graph.edges[(predecessor_id, node_id)]['relation'] == 0:
                    pass
                else:
                    raise ValueError(
                        f"Unknown type of relation {graph.edges[(predecessor_id, node_id)]['relation']}. Relation "
                        f"should be 1 for support and -1 for attack."
                    )

            # Computing attackers score based on node initial acceptability and attackers' final acceptabilities
            if attackers:
                attackers_score = initial_acceptability[node_id] * np.prod(
                    [1 - final_acceptability[attacker_id] for attacker_id in attackers])
            else:
                attackers_score = None

            # Compute supporters score based on node initial acceptability and supporters' final acceptabilities
            if supporters:
                supporters_score = 1 - (1 - initial_acceptability[node_id]) * np.prod(
                    [1 - final_acceptability[supporter_id] for supporter_id in supporters])
            else:
                supporters_score = None

            # Computing node final acceptability given attackers' and supporters' scores
            if attackers_score is None and supporters_score is None:
                updated_acceptability = initial_acceptability[node_id]
            elif supporters_score is None and attackers_score is not None:
                updated_acceptability = attackers_score
            elif supporters_score is not None and attackers_score is None:
                updated_acceptability = supporters_score
            else:
                updated_acceptability = (attackers_score + supporters_score) / 2.

            # Check if node final acceptability has changed since previous round
            if abs(final_acceptability[node_id] - updated_acceptability) >= eps:
                temp_acceptability[node_id] = updated_acceptability
                has_changed = True
            else:
                temp_acceptability[node_id] = final_acceptability[node_id]

        # Update final acceptabilities with new computed ones
        final_acceptability = temp_acceptability.copy()

    return final_acceptability


def df_quad_semantics(graph: nx.DiGraph, initial_acceptability: Dict[Any, float],
                      eps: float = 1e-4) -> Dict[Any, float]:
    """
    Compute Discontinuity-Free Quantitative Argumentation Debate semantics from [1]_.

    Parameters
    ----------
    graph: nx.DiGraph
        Debate graph.

    initial_acceptability: dict
        Dictionary of initial nodes acceptabilities. Keys must be node IDs, and values are floats between 0 and 1.

    eps: float, default=1e-4
        Threshold used to determine if the final acceptability has converged.

    Returns
    -------
    final_acceptability: dict
        Dictionary of final nodes acceptabilities. Keys are the same as `initial_acceptability`, and values are floats
        between 0 and 1.

    References
    ----------

    .. [1] Rago et al., Discontinuity-free decision support with quantitative argumentation debates, 2016.
    """
    final_acceptability = initial_acceptability.copy()
    has_changed = True

    while has_changed:
        temp_acceptability = {}
        has_changed = False

        for node_id in nx.topological_sort(graph):
            # Get lists of node attackers et supporters
            attackers = []
            supporters = []
            for predecessor_id in graph.predecessors(node_id):
                if graph.edges[(predecessor_id, node_id)]['relation'] == -1:
                    attackers.append(predecessor_id)
                elif graph.edges[(predecessor_id, node_id)]['relation'] == 1:
                    supporters.append(predecessor_id)
                elif graph.edges[(predecessor_id, node_id)]['relation'] == 0:
                    pass
                else:
                    raise ValueError(
                        f"Unknown type of relation {graph.edges[(predecessor_id, node_id)]['relation']}. Relation "
                        f"should be 1 for support and -1 for attack.")

            # Computing attackers score based on node initial acceptability and attackers' final acceptabilities
            attackers_score = 1 - np.prod([1 - final_acceptability[attacker_id] for attacker_id in attackers])
            # Compute supporters score based on node initial acceptability and supporters' final acceptabilities
            supporters_score = 1 - np.prod([1 - final_acceptability[supporter_id] for supporter_id in supporters])

            # Computing node final acceptability given attackers' and supporters' scores
            if attackers_score >= supporters_score:
                updated_acceptability = initial_acceptability[node_id] - initial_acceptability[node_id] * abs(
                    supporters_score - attackers_score)
            else:
                updated_acceptability = initial_acceptability[node_id] + (1 - initial_acceptability[node_id]) * abs(
                    supporters_score - attackers_score)

            # Check if node final acceptability has changed since previous round
            if abs(final_acceptability[node_id] - updated_acceptability) >= eps:
                temp_acceptability[node_id] = updated_acceptability
                has_changed = True
            else:
                temp_acceptability[node_id] = final_acceptability[node_id]

        # Update final acceptabilities with new computed ones
        final_acceptability = temp_acceptability.copy()

    return final_acceptability


def exponent_based_semantics(graph: nx.DiGraph, initial_acceptability: Dict[Any, float]) -> Dict[Any, float]:
    """
    Compute exponent based semantics from [1]_.

    Parameters
    ----------
    graph: nx.DiGraph
        Debate graph.

    initial_acceptability: dict
        Dictionary of initial nodes acceptabilities. Keys must be node IDs, and values are floats between 0 and 1.

    Returns
    -------
    final_acceptability: dict
        Dictionary of final nodes acceptabilities. Keys are the same as `initial_acceptability`, and values are floats
        between 0 and 1.

    References
    ----------

    .. [1] Amgoud and Ben-Naim, Evaluation of arguments in weighted bipolar graphs, 2018.
    """
    final_acceptability = {}

    for node_id in nx.topological_sort(graph):
        # Computing exponent by adding supporters' final acceptability and subtracting attackers'.
        exponent = 0
        for predecessor_id in graph.predecessors(node_id):
            exponent += graph.edges[(predecessor_id, node_id)]['relation'] * final_acceptability[predecessor_id]

        # Computing node final acceptability given exponent
        final_acceptability[node_id] = 1. - (1. - initial_acceptability[node_id] ** 2) / (
                1. + initial_acceptability[node_id] * 2 ** exponent)

    return final_acceptability


def quem_semantics(graph: nx.DiGraph, initial_acceptability: Dict[Any, float]):
    """
    Compute Quantitative Energy Model semantics from [1]_.

    Parameters
    ----------
    graph: nx.DiGraph
        Debate graph.

    initial_acceptability: dict
        Dictionary of initial nodes acceptabilities. Keys must be node IDs, and values are floats between 0 and 1.

    Returns
    -------
    final_acceptability: dict
        Dictionary of final nodes acceptabilities. Keys are the same as `initial_acceptability`, and values are floats
        between 0 and 1.

    References
    ----------

    .. [1] Potyka, Continuous Dynamical Systems for Weighted Bipolar Argumentation, 2018.
    """
    final_acceptability = {}

    for node_id in nx.topological_sort(graph):
        node_predecessors = list(graph.predecessors(node_id))
        # if the argument has no attackers or supporters, acceptability will stay the same.
        if not node_predecessors:
            final_acceptability[node_id] = initial_acceptability[node_id]
        else:
            # Impact function
            def h(e):
                return (max(e, 0) ** 2) / (1 + max(e, 0) ** 2)

            # Compute node energy by adding supporters' final acceptability and subtracting attackers'.
            node_energy = 0
            for predecessor_id in node_predecessors:
                node_energy += graph.edges[(predecessor_id, node_id)]['relation'] * final_acceptability[predecessor_id]

            node_weight = initial_acceptability[node_id]

            # Computing final acceptability according to quadratic energy model
            if node_energy > 0:
                final_acceptability[node_id] = node_weight + (1 - node_weight) * h(node_energy)
            else:
                final_acceptability[node_id] = node_weight - node_weight * h(-node_energy)

    return final_acceptability


if __name__ == '__main__':
    ...
