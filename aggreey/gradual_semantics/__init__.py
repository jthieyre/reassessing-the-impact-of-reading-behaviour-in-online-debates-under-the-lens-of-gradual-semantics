# -*- coding: utf-8 -*-

# imports
from ._bipolar_gradued_semantics import quad_semantics, df_quad_semantics, exponent_based_semantics, quem_semantics

__all__ = [
    'quad_semantics',
    'df_quad_semantics',
    'exponent_based_semantics',
    'quem_semantics'
]
