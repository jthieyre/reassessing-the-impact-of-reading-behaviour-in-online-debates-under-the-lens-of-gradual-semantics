# Reassessing the Impact of Reading Behaviour in Online Debates Under the Lens of Gradual Semantics

This repository provides the full code and notebooks used to produce the results of the paper "Reassessing the Impact of Reading Behaviour in Online Debates Under the Lens of Gradual Semantics" published in Proceedings of the Fifth International Workshop on Systems and Algorithms for Formal Argumentation co-located with 10th International Conference on Computational Models of Argument (COMMA 2024). Paper can be found here:
- [https://ceur-ws.org/Vol-3757/paper9.pdf](https://ceur-ws.org/Vol-3757/paper9.pdf)
- [https://cnrs.hal.science/LIP6/hal-04715607v1](https://cnrs.hal.science/LIP6/hal-04715607v1)

## Requirements

Code and notebook work with Python version 3.10.12. Besides, following packages are also required and can be installed using the command:
```
pip3 install networkx==3.2.1 numpy==1.26.4 pandas==2.2.0 scipy==1.12.0 scikit-learn==1.4.0
```
## Aggreey package

The `aggreey` folder provides a small Python package for gradual semantics, namely QuAD [1], DF-QuAD [2], Exponent-based [3] and QuEM [4].

## Data

The `data` folder provides all the scraped Kialo debates data as JSON files. Function `load_kialo_graph_data` from module `aggreey.utils.data` can be used to load a JSON file in the form of a NetworkX graph.

## Usage

Simulations can take time despite the use of Python `multiprocessing`. In [reading_behaviour_analysis-max_depth=1-same_node_start.csv.zip](./reading_behaviour_analysis-max_depth=1-same_node_start.csv.zip) we provide the results for the following behaviours, semantics and initial score method :
- Behaviours :
    - Chronological (NT+CO in the paper),
    - Normalized descending likes,
    - Not normalized descending likes,
    - Depth First Search + chronological (DFS+CO in the paper),
    - DFS + normalized descending likes (DFS+DL in the paper),
    - DFS + not normalized descending likes,
    - DFS + normalized descending likes with PRO-CON diversity (DFS+PCD in the paper),
    - Breadth First Search + descending normalized likes,
    - BFS + normalized descending likes with PRO-CON diversity (BFS+PCD in the paper),
    - BFS + controversy (= descending vote distribution variance),
    - Hybrid traversal + normalized descending likes,
    - HT + normalized descending likes with PRO-CON diversity (HT+PCD in the paper),
    - HT + controversy,
    - HT with max depth of 2 + normalized descending likes with PRO-CON diversity,
    - HT with max depth of 3 + normalized descending likes with PRO-CON diversity (MDHT+PCD in the paper),
    - HT with max depth of 4 + normalized descending likes with PRO-CON diversity,
    - HT with max depth of 5 + normalized descending likes with PRO-CON diversity,
    - PRO dogmatic (= supports of central question, supports of latter supports, and so on) + BFS + normalized descending likes with PRO-CON diversity,
    - CON dogmatic (= attacks of central question, supports of latter attacks, supports of latter supports, and so on) + BFS + normalized descending likes with PRO-CON diversity,
- Semantics:
    - QuAD [1],
    - DF-QuAD [2],
    - Exponent-based [3],
    - QuEM [4],
- Initial score method:
    - Same initial score of 0.5 for all arguments,
    - Using votes (see Section 3.4 of the paper).

If you want to explore other possibilities, the simulation code can be executed using the following command :
```
python -m Reading_behaviour_simulations
```
## Analysis

Finally, notebook [Reading_behaviour_analysis.ipynb](./Reading_behaviour_analysis.ipynb) provides code and interactive plots to explore simulations results.

## References

[1] P. Baroni, M. Romano, F. Toni, M. Aurisicchio, and G. Bertanza. Automatic evaluation of design alternatives with quantitative argumentation. Argument & Computation, 6(1):24–49, Jan. 2015.

[2] A. Rago, F. Toni, M. Aurisicchio, and P. Baroni. Discontinuity-free decision support with quantitative argumentation debates. In Proceedings of the Fifteenth International Conference on Principles of Knowledge Representation and Reasoning, KR’16, pages 63–72, Cape Town, South Africa, Apr. 2016. AAAI Press.

[3] L. Amgoud and J. Ben-Naim. Evaluation of arguments in weighted bipolar graphs. International Journal of Approximate Reasoning, 99:39–55, Aug. 2018.

[4] N. Potyka. Continuous dynamical systems for weighted bipolar argumentation. In M. Thielscher, F. Toni, and F. Wolter, editors, Principles of Knowledge Representation and Reasoning: Proceedings of the Sixteenth International Conference, KR 2018, pages 148–157. AAAI Press, 2018.
