# imports
from aggreey.gradual_semantics import quad_semantics, df_quad_semantics, exponent_based_semantics, quem_semantics
from aggreey.utils.data import load_kialo_graph_data

from datetime import datetime
from itertools import chain, zip_longest
from multiprocessing import Pool
from scipy.stats import weightedtau
from sklearn.metrics import root_mean_squared_error
from typing import Any, Callable, Dict, List, Optional, Union

import networkx as nx
import numpy as np
import os
import pandas as pd
import time
import warnings


# functions
def humanize_duration(duration: int) -> str:
    base_unit = [
        (60, 's'),
        (60, 'min'),
        (24, 'h'),
        (365, 'day'),
        (1, 'year')
    ]

    humanized_duration = ''

    i = 0
    while duration != 0:
        if i > 0:
            humanized_duration = ' ' + humanized_duration
        humanized_duration = f'{duration % base_unit[i][0]} {base_unit[i][1]}' + humanized_duration
        duration = duration // base_unit[i][0]
        i += 1

    return humanized_duration


def votes_to_value(votes: Union[List[Union[int, float]], np.ndarray],
                   weights: Union[List[Union[int, float]], np.ndarray], normalize: bool = True) -> Union[int, float]:
    if len(votes) != len(weights):
        raise ValueError(f'`votes` must be of the same lenght as `weights`: {len(votes)} != {len(weights)}.')

    if sum(votes) == 0:
        return 0.5
    else:
        value = 0
        for v, w in zip(votes, weights):
            value += v * w

        if normalize:
            return value / sum(votes)
        else:
            return value


def chronological_ranking(graph: nx.DiGraph, time_key: str = 'created') -> List[Any]:
    node_time = np.zeros((len(graph.nodes), 2), dtype=object)

    for i, (node_id, node_values) in enumerate(graph.nodes.items()):
        node_time[i, 0] = node_id
        node_time[i, 1] = node_values[time_key]

    return node_time[np.argsort(node_time[:, 1]), 0].tolist()


def likes_ranking(graph: nx.DiGraph, **kwargs) -> List[Any]:
    node_like = np.zeros((len(graph.nodes), 2), dtype=object)

    for i, (node_id, node_values) in enumerate(graph.nodes.items()):
        node_like[i, 0] = node_id
        node_like[i, 1] = votes_to_value(node_values['votes'], **kwargs)

    return node_like[np.argsort(node_like[:, 1])[::-1], 0].tolist()


def pro_con_diversity_ranking(graph: nx.DiGraph, whole_graph: nx.DiGraph, **kwargs) -> List[Any]:
    node_like_rel = np.zeros((len(graph.nodes), 3), dtype=object)

    for i, (node_id, node_values) in enumerate(graph.nodes.items()):
        node_like_rel[i, 0] = node_id
        node_like_rel[i, 1] = votes_to_value(node_values['votes'], **kwargs)
        rel_type = whole_graph.edges[(node_id, list(whole_graph.successors(node_id))[0])]['relation']
        if rel_type == -1.:
            node_like_rel[i, 2] = 'CON'
        else:
            node_like_rel[i, 2] = 'PRO'

    pros = node_like_rel[node_like_rel[:, 2] == 'PRO']
    cons = node_like_rel[node_like_rel[:, 2] == 'CON']

    sorted_pros = pros[np.argsort(pros[:, 1])[::-1], 0]
    sorted_cons = cons[np.argsort(cons[:, 1])[::-1], 0]

    return [x for x in chain.from_iterable(zip_longest(sorted_pros, sorted_cons)) if x is not None]


def controversial_ranking(graph: nx.DiGraph) -> List[Any]:
    node_like = np.zeros((len(graph.nodes), 2), dtype=object)

    for i, (node_id, node_values) in enumerate(graph.nodes.items()):
        node_like[i, 0] = node_id
        node_like[i, 1] = np.var(node_values['votes'])

    return node_like[np.argsort(node_like[:, 1])[::-1], 0].tolist()


def depth_first_search_behaviour(graph: nx.DiGraph, source: Any, ranking_policy: Callable,
                                 max_depth: Optional[int] = None, **kwargs) -> List[Any]:
    already_visited_nodes = []
    node_stack = [source]
    node_sorting = []

    while node_stack:
        current_node_id = node_stack.pop()

        already_visited_nodes.append(current_node_id)

        node_sorting.append(current_node_id)

        for node in ranking_policy(graph.subgraph(graph.predecessors(current_node_id)), **kwargs)[::-1]:
            if node not in already_visited_nodes:
                node_stack.append(node)

    if max_depth:
        shortest_path_lengths = nx.shortest_path_length(graph.to_undirected(), source)

        first_part = []
        second_part = []
        for node_id in node_sorting:
            if shortest_path_lengths[node_id] <= max_depth:
                first_part.append(node_id)
            else:
                second_part.append(node_id)

        node_sorting = first_part + second_part

    return node_sorting


def breadth_first_search_behaviour(graph: nx.DiGraph, source: Any, ranking_policy: Callable, **kwargs) -> List[Any]:
    already_visited_nodes = []
    node_queue = [source]
    node_sorting = []

    while node_queue:
        current_node_id = node_queue.pop(0)

        already_visited_nodes.append(current_node_id)

        node_sorting.append(current_node_id)

        for node in ranking_policy(graph.subgraph(graph.predecessors(current_node_id)), **kwargs):
            if node not in already_visited_nodes:
                node_queue.append(node)

    return node_sorting


def hybrid_search_behaviour(graph: nx.DiGraph, source: Any, ranking_policy: Callable,
                            max_depth: Optional[int] = None, **kwargs) -> List[Any]:
    already_visited_nodes = []
    node_stack = [source]
    node_sorting = []

    while node_stack:
        current_node_id = node_stack.pop()

        already_visited_nodes.append(current_node_id)

        sorted_predecessors = ranking_policy(graph.subgraph(graph.predecessors(current_node_id)), **kwargs)

        node_sorting += sorted_predecessors

        for node in sorted_predecessors[::-1]:
            if node not in already_visited_nodes:
                node_stack.append(node)

    if max_depth:
        shortest_path_lengths = nx.shortest_path_length(graph.to_undirected(), source)

        first_part = []
        second_part = []
        for node_id in node_sorting:
            if shortest_path_lengths[node_id] <= max_depth:
                first_part.append(node_id)
            else:
                second_part.append(node_id)

        node_sorting = first_part + second_part

    return node_sorting


def dogmatic_behaviour(graph: nx.DiGraph, source: Any, dogmatic_opinion: Any, behaviour: Callable, **kwargs):
    already_visited_nodes = []
    node_queue = [source]
    dogmatic_opinion_node = []

    i = 0
    while node_queue:
        current_node_id = node_queue.pop(0)

        already_visited_nodes.append(current_node_id)

        dogmatic_opinion_node.append(current_node_id)

        for node in graph.subgraph(graph.predecessors(current_node_id)):
            if i == 0:
                if node not in already_visited_nodes and graph.edges[(node, current_node_id)]['relation'] == dogmatic_opinion:
                    node_queue.append(node)
            else:
                if node not in already_visited_nodes and graph.edges[(node, current_node_id)]['relation'] == 1.:
                    node_queue.append(node)
        i += 1

    return behaviour(graph.subgraph(dogmatic_opinion_node), source, **kwargs)


def behaviour_comparison(filename: str, question_type: str, graph: nx.DiGraph, initial_acceptability_method: str,
                         initial_acceptability: Dict[Any, float], semantics: Callable,
                         final_acceptability: Dict[Any, float], node_depth: Dict[Any, int],
                         node_importance: Dict[Any, float], behaviour_name: str, sorted_node_id: List[Any],
                         first_node_size: int) -> List[Any]:
    comparisons = []

    for n in range(1, len(sorted_node_id) + 1):
        subgraph_final_acceptability = semantics(graph.subgraph(sorted_node_id[:n]),
                                                 initial_acceptability)

        node_importance_weights = [node_importance[node_id] for node_id in subgraph_final_acceptability.keys()]

        tau = (1 - weightedtau(
            list(subgraph_final_acceptability.values()),
            [final_acceptability[node_id] for node_id in subgraph_final_acceptability.keys()],
            rank=False,
            weigher=lambda r: node_importance_weights[r],
            additive=False
        ).statistic) / 2
        if np.isnan(tau):
            tau = 1.

        rmse = root_mean_squared_error(
            list(subgraph_final_acceptability.values()),
            [final_acceptability[node_id] for node_id in subgraph_final_acceptability.keys()],
            sample_weight=node_importance_weights,
        )

        subgraph_accepted = [node_id for node_id, acceptability in subgraph_final_acceptability.items() if
                             acceptability > .5]
        graph_accepted = [node_id for node_id in subgraph_final_acceptability.keys() if
                          final_acceptability[node_id] > .5]

        intersection = set(subgraph_accepted).intersection(graph_accepted)
        union = set(subgraph_accepted).union(graph_accepted)
        try:
            jaccard_index = sum([node_importance[node_id] for node_id in intersection]) / sum(
                [node_importance[node_id] for node_id in union])
        except ZeroDivisionError:
            jaccard_index = 1.0

        comparisons.append([filename, question_type, initial_acceptability_method, semantics.__name__,
                            behaviour_name, first_node_size, n, sorted_node_id[n - 1],
                            node_depth[sorted_node_id[n - 1]],
                            final_acceptability[sorted_node_id[n - 1]], tau, rmse, jaccard_index])

    return comparisons


# Comparison of policies : Kendall rank correlation coefficient and root mean squared error between computed
# acceptabilities of whole graph and policy given subgraph.
if __name__ == '__main__':
    print(f"Starting at {datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M')}")
    t0 = time.time()

    warnings.filterwarnings('error')

    semantics_list = [
        quad_semantics,
        df_quad_semantics,
        exponent_based_semantics,
        quem_semantics,
    ]

    # Parameter for node importance
    # Exponential decay factor. Will be used to compute the weights for weighted metrics. 0 means all weights will be 1.
    # decay_factor = 1.

    # Depth beyond which weights will be worth 0
    max_depth = 1

    print("Creating pool of workers")
    pool = Pool(processes=os.cpu_count() - 1)
    results = []

    print("Creating processes")
    for filename in os.listdir('./data'):
        # Load data as a NetworkX directed graph
        graph = load_kialo_graph_data(f'./data/{filename}')

        # Check if graph is acyclic because semantics don't always work on acyclic graphs
        if nx.is_directed_acyclic_graph(graph):
            # Set root node
            root_node_id = list(nx.topological_sort(graph))[-1]

            # Guess question type
            if root_node_id[-2:] == '.0':  # If root node ID end by .0, then it's an open-ended question.
                question_type = 'open-ended'
            else:
                question_type = 'closed-ended'  # Else it's closed-ended

            # Define tested policies and their parameters
            reading_behaviours = [
                (
                    'chronological_behaviour',
                    chronological_ranking,
                    {'time_key': 'created'}
                ),
                (
                    'likes_normalized_behaviour',
                    likes_ranking,
                    {'weights': [0., .25, .5, .75, 1.]}
                ),
                (
                    'likes_not_normalized_behaviour',
                    likes_ranking,
                    {
                        'weights': [0., .25, .5, .75, 1.],
                        'normalize': False
                    }
                ),
                (
                    'depth_first_search_behaviour+chronological_ranking',
                    depth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': chronological_ranking,
                        'time_key': 'created'
                    }
                ),
                (
                    'depth_first_search_behaviour+likes_normalized_ranking',
                    depth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': likes_ranking,
                        'weights': [0., .25, .5, .75, 1.]
                    }
                ),
                (
                    'depth_first_search_behaviour+likes_not_normalized_ranking',
                    depth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': likes_ranking,
                        'weights': [0., .25, .5, .75, 1.],
                        'normalize': False
                    }
                ),
                (
                    'depth_first_search_behaviour+pro_con_diversity_ranking',
                    depth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'breadth_first_search_behaviour+likes_normalized_ranking',
                    breadth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': likes_ranking,
                        'weights': [0., .25, .5, .75, 1.],
                        'normalize': False
                    }
                ),
                (
                    'breadth_first_search_behaviour+pro_con_diversity_ranking',
                    breadth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'breadth_first_search_behaviour+controversial_ranking',
                    breadth_first_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': controversial_ranking,
                    }
                ),
                (
                    'hybrid_search_behaviour+likes_normalized_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': likes_ranking,
                        'weights': [0., .25, .5, .75, 1.],
                        'normalize': False
                    }
                ),
                (
                    'hybrid_search_behaviour+pro_con_diversity_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'hybrid_search_behaviour+controversial_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': controversial_ranking,
                    }
                ),
                (
                    'max_depth_2_hybrid_search_behaviour+pro_con_diversity_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'max_depth': 2,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'max_depth_3_hybrid_search_behaviour+pro_con_diversity_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'max_depth': 3,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'max_depth_4_hybrid_search_behaviour+pro_con_diversity_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'max_depth': 4,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'max_depth_5_hybrid_search_behaviour+pro_con_diversity_ranking',
                    hybrid_search_behaviour,
                    {
                        'source': root_node_id,
                        'ranking_policy': pro_con_diversity_ranking,
                        'max_depth': 5,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'dogmatic_pro_bfs_behaviour+pro_con_diversity_ranking',
                    dogmatic_behaviour,
                    {
                        'source': root_node_id,
                        'dogmatic_opinion': 1.0,
                        'behaviour': breadth_first_search_behaviour,
                        'ranking_policy': pro_con_diversity_ranking,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
                (
                    'dogmatic_con_bfs_behaviour+pro_con_diversity_ranking',
                    dogmatic_behaviour,
                    {
                        'source': root_node_id,
                        'dogmatic_opinion': -1.0,
                        'behaviour': breadth_first_search_behaviour,
                        'ranking_policy': pro_con_diversity_ranking,
                        'whole_graph': graph,
                        'weights': [0., .25, .5, .75, 1.],
                    }
                ),
            ]

            # Node importance
            # Shortest distance from root node
            shortest_path_lengths = nx.shortest_path_length(graph.to_undirected(), root_node_id)

            # node_importance = {node_id: np.exp(-decay_factor * path_length) for node_id, path_length in
            #                    shortest_path_lengths.items()}  # Exponential decay with distance from root node

            node_importance = {node_id: 1 if path_length <= max_depth else 0 for node_id, path_length in
                               shortest_path_lengths.items()}  # Max depth importance

            # Computing initial acceptabilities in several ways
            initial_acceptabilities = [
                # All arguments have the same initial acceptability of 0.5
                ('same initial acceptability', {node_id: .5 for node_id in graph.nodes}),
                # Use votes on arguments to compute acceptabilities
                ('votes to value',
                 {node_id: votes_to_value(graph.nodes[node_id]['votes'], [0., .25, .5, .75, 1.]) for node_id in
                  graph.nodes})
            ]

            for method_name, initial_acceptability in initial_acceptabilities:
                for semantics in semantics_list:
                    final_acceptability = semantics(graph, initial_acceptability)

                    for behaviour_name, reading_behaviour, kwargs in reading_behaviours:
                        sorted_node_id = np.array(reading_behaviour(graph, **kwargs))

                        # Force behaviour to start with root node and his predecessors
                        first_node_id = np.array([root_node_id] + pro_con_diversity_ranking(
                            graph.subgraph(graph.predecessors(root_node_id)), graph, weights=[0., .25, .5, .75, 1.]))

                        sorted_node_id = np.concatenate([
                            first_node_id,
                            sorted_node_id[~np.isin(sorted_node_id, first_node_id)]
                        ]).tolist()

                        results.append(
                            pool.apply_async(
                                behaviour_comparison,
                                (filename, question_type, graph, method_name, initial_acceptability, semantics,
                                 final_acceptability, shortest_path_lengths, node_importance, behaviour_name,
                                 sorted_node_id, first_node_id.shape[0])
                            )
                        )

    print('Closing pool')
    pool.close()

    print('Waiting for processes to finish')
    pool.join()

    print('Getting results')
    results = np.concatenate([res.get() for res in results], axis=0)

    print('Saving results on disk')
    pd.DataFrame(
        results,
        columns=['file_name', 'question_type', 'initial_acceptability_method', 'semantics', 'reading_behaviour',
                 'first_node_size', 'size_of_subgraph_read', 'added_node_id', 'added_node_depth',
                 'added_node_finale_acceptability', 'kendall-tau', 'rmse', 'jaccard_index']
    ).to_csv(
        'new_reading_behaviour_analysis-max_depth=1-same_node_start.csv.zip',
        index=False,
        sep=';',
        encoding='utf-8',
        compression='zip'
    )

    print(f'Done in {humanize_duration(int(time.time() - t0))}')
